const PREFIX = "./images";

function fileRef(path) {
  return `${PREFIX}/${path}`;
}

/// Keep dictionary order!
export const avatar = fileRef("avatar@1.5x.jpg");
export const contentProductImage = fileRef("content_product_image@1.5x.jpg");
export const directShopBadge = fileRef("direct_shop_badge@1.5.png");
export const iconAlarm = fileRef("icon_alarm@1.5x.png");
export const iconArrow = fileRef("icon_arrow@1.5x.png");
export const logo = fileRef("logo@3x.png");
export const productImage = fileRef("product_image@1.5x.jpg");

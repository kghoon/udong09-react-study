import dayjs from "dayjs";
import "dayjs/locale/ko";

import { useQuery } from "react-query";
import { QuillDeltaToHtmlConverter } from "quill-delta-to-html";
import { getProductDetails } from "src/api/products";
import { DeliveryInfo, PickupInfo, ProductDetails, ProductLabelling } from "src/types/ProductDetails";

dayjs.extend(require("dayjs/plugin/localizedFormat"));
dayjs.locale("ko");

export function useProductDetails() {
  const queryKey = "getProductDetails";

  return useQuery<ProductDetails>([queryKey], async () => {
    const data = await getProductDetails();
    return makeViewModel(data);
  });
}

function makeViewModel(data: any): ProductDetails {
  return {
    photos: data.photo,
    seller: {
      nickname: data.nickname,
      image: {
        thumbnail: data.profile_photo?.thumb,
        large: data.profile_photo?.main,
      },
    },
    deliverySummary: makeDeliverySummary(data),
    title: data.title,
    deliveryMethods: data.delivery_method,
    purchaseCount: data.purchase_count || 0,
    viewCount: data.productViewCount || 0,
    priceText: makePriceText(data),
    pickupInfo: makePickupInfo(data),
    deliveryInfo: makeDeliveryInfo(data),
    optionsText: makeOptionsText(data),
    htmlDescription: makeHtmlDescription(data),
    labelling: makeLabelling(data),
  };
}

function makeDeliverySummary(data: any): string {
  if (data.delivery_method.includes("pickup")) {
    const [pickup] = data.pickup;
    if (pickup.location.direct_shop) {
      return "매장 픽업 상품";
    } else {
      return "픽업 상품";
    }
  } else if (data.delivery_method.includes("delivery")) {
    return "배송 상품";
  } else {
    return "";
  }
}

function makePriceText(data: any): string {
  const { option: options } = data;

  if (options.length == 0) {
    return "";
  } else if (options.length === 1) {
    const [option] = options;
    const price = (option.price + option.fee).toLocaleString();
    return `${price}원`;
  } else {
    const [cheapest] = options.sort((x: any, y: any) => {
      const priceX = x.price + x.fee;
      const priceY = y.price + y.fee;

      return priceY - priceX;
    });

    const price = (cheapest.price + cheapest.fee).toLocaleString();

    return `${price}원~`;
  }
}

function makePickupInfo(data: any): PickupInfo | undefined {
  if (!data.pickup) {
    return undefined;
  }

  const [pickup] = data.pickup;
  const startTime = dayjs(pickup.start_time);
  const endTime = dayjs(pickup.end_time);

  const dateText = startTime.format("YYYY.MM.DD (dd)");
  const startDateTimeText = startTime.format("YYYY년 MM월 DD일 HH시 mm분");
  const endTimeText = endTime.format("HH시 mm분");
  const locations = data.pickup.map((pickup: any) => pickup.location.title.replace("우동공구 ", "")).join(", ");

  let shortDateTimeDuration = startTime.format("MM월 DD일");
  shortDateTimeDuration += startTime.format(" A h시");
  if (startTime.minute() > 0) {
    shortDateTimeDuration += startTime.format(" m분");
  }
  shortDateTimeDuration += " ~ ";
  if (startTime.format("A") !== endTime.format("A")) {
    shortDateTimeDuration += endTime.format("A ");
  }
  shortDateTimeDuration += endTime.format("h시 ");
  if (endTime.minute() > 0) {
    shortDateTimeDuration += endTime.format("m분");
  }

  return {
    date: dateText,
    dateTimeDuration: `${startDateTimeText} ~ ${endTimeText}`,
    shortDateTimeDuration: shortDateTimeDuration,
    location: pickup.location.title,
    locations,
    note: data.pickup_caption || "",
  };
}

function makeDeliveryInfo(data: any): DeliveryInfo | undefined {
  const { delivery } = data;
  if (!delivery) return undefined;

  const startTime = dayjs(delivery.delivery_date.start_time);
  const endTime = dayjs(delivery.delivery_date.end_time);

  let region = "";
  if (delivery.regions.some((region: any) => region.code === "0000000000")) {
    region = "전국배송상품";
  } else {
    region = delivery.regions.map((region: any) => region.address_name).join(", ");
  }

  return {
    dateDuration: startTime.format("MM월 DD일") + " ~ " + endTime.format("MM월 DD일"),
    region: region,
    note: delivery.caption,
  };
}

function makeOptionsText(data: any): string[] {
  const lines = data.option.map((option: any) => {
    const price = (option.price + option.fee).toLocaleString() + "원";
    return `${option.name} ${price}`;
  });

  return lines;
}

function makeHtmlDescription(data: any): string {
  const delta = JSON.parse(data.desc);
  const converter = new QuillDeltaToHtmlConverter(delta);
  const result = converter.convert();

  console.log("makeHtmlDescription", { result });

  return result;
}

type LabelKey = keyof ProductLabelling;
type LabelKeyNameMap = {
  [key in LabelKey]: string;
};

function makeLabelling(data: any): Array<[string, string]> {
  const labelKeyMap: LabelKeyNameMap = {
    weight: "중량/갯수",
    expiration: "보관 및 섭취기한",
    manufactureDate: "제조일자",
    manufactureCountry: "제조국",
    details: "기타",
  };

  const list: Array<[string, string]> = [];
  if (data.labelling) {
    for (const k of Object.keys(data.labelling)) {
      const v = data.labelling[k];
      if (v && v.length > 0) {
        list.push([labelKeyMap[k as LabelKey], v]);
      }
    }
  }
  return list;
}

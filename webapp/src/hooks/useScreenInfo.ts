import { useState, useEffect, useCallback } from "react";

enum ScreenType {
  Desktop = 0,
  MediumDevice = 1,
  Mobile = 2,
}

export interface UseScreenInfoResult {
  isDesktop: boolean;
  isMediumDevice: boolean;
  isMobile: boolean;
  appViewWidth: number;
}

function useScreenInfo(): UseScreenInfoResult {
  const [deviceType, setDeviceType] = useState<ScreenType>(ScreenType.Desktop);
  const [appViewWidth, setAppViewWidth] = useState<number>();

  const handleResize = useCallback(() => {
    updateScreenType();
    updateAppViewWidth();
  }, []);

  function updateScreenType() {
    const { innerWidth: width } = window;

    if (width >= 1200) {
      setDeviceType(ScreenType.Desktop);
    } else if (width > 640) {
      setDeviceType(ScreenType.MediumDevice);
    } else {
      setDeviceType(ScreenType.Mobile);
    }
  }

  function updateAppViewWidth() {
    const { innerWidth: width } = window;
    const calculatedWidth = width > 640 ? 640 : width >= 340 ? width : 340;
    setAppViewWidth(calculatedWidth);
  }

  useEffect(() => {
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [handleResize]);

  return {
    isDesktop: deviceType === ScreenType.Desktop,
    isMediumDevice: deviceType === ScreenType.MediumDevice,
    isMobile: deviceType === ScreenType.Mobile,
    appViewWidth: appViewWidth ?? 0,
  };
}

export default useScreenInfo;

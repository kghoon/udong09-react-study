import { AnyStyledComponent } from "styled-components";

export interface Styles {
  [key: string]: AnyStyledComponent;
}

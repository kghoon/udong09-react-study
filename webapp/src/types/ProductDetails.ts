import { DeliveryMethod } from "./DeliveryMethod";

export interface ProductDetails {
  photos: string[];
  seller: {
    nickname: string;
    image: {
      thumbnail?: string;
      large?: string;
    };
  };
  deliverySummary: string;
  title: string;
  deliveryMethods: DeliveryMethod[];
  purchaseCount: number;
  viewCount: number;
  priceText: string;
  pickupInfo?: PickupInfo;
  deliveryInfo?: DeliveryInfo;
  htmlDescription: string;
  optionsText: string[];
  labelling: Array<[string, string]>;
}

export interface PickupInfo {
  date: string;
  dateTimeDuration: string;
  shortDateTimeDuration: string;
  location: string;
  locations: string;
  note: string;
}

export interface DeliveryInfo {
  dateDuration: string;
  region: string;
  note: string;
}

export interface ProductLabelling {
  weight?: string;
  expiration?: string;
  manufactureDate?: string;
  manufactureCountry?: string;
  details?: string;
}

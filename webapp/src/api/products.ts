import axios from "axios";

export async function getProductDetails(productId?: string) {
  // productId ??= "7f0bb4b0-6c63-45e4-bd4b-a381f95b6370";
  productId ??= "9322a6a0-795e-40ec-add9-d54946b31b8f";
  productId = "bdb85a1b-6c5c-4a26-9b6d-d7bdec1c3f1c";
  const resp = await axios.get(
    `https://api-server-seq5ei74uq-du.a.run.app/market/products/${productId}?api_key=036d643f2bb14c7ea94e0952d377965d`
  );

  return resp.data?.result;
}

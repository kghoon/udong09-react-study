import styled from "styled-components";

export const Divider = styled.div`
  height: 0px;
  width: 100%;
  border-top: 0.1px solid rgba(0, 0, 0, 0.1);
`;

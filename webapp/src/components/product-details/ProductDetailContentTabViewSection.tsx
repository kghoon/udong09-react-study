import styled from "styled-components";
import { useProductDetails } from "src/hooks/useProductDetails";
import { Styles } from "src/types/Styles";
import { ProductDescriptionBox } from "./ProductDescriptionBox";
import { ProductDetails } from "src/types/ProductDetails";

const S: Styles = {
  ...require("../WTexts"),
  ...require("../SizedBox"),
  ...require("../Divider"),
};

S.ProductDetailTabViewHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding: 0px 20px;
`;

S.ProductDetailTabViewItem = styled.div<{ active?: boolean }>`
  height: 27px;

  /* Title 03 */
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 27px;
  /* identical to box height */
  padding-bottom: 28px;

  text-align: center;
  border-bottom: 3px solid #36b190;
  border-bottom-width: ${(props) => (props.active ? "3px" : "0px")};
`;

S.ProductDetailTabContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 100%;

  padding: 39px 20px;
`;

S.SubtitleItem = styled.div`
  display: flex;

  height: 27px;

  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 27px;

  text-align: center;
  border-bottom: 3px solid #2c0e02;
`;

export const ProductDetailContentTabViewSection = () => {
  const { isLoading, data: product } = useProductDetails();

  if (isLoading || !product) return <></>;

  return (
    <section>
      <S.ProductDetailTabViewHeader>
        <S.ProductDetailTabViewItem active>상세정보</S.ProductDetailTabViewItem>
        <S.SizedBox width="36px" />
        <S.ProductDetailTabViewItem>댓글 0</S.ProductDetailTabViewItem>
      </S.ProductDetailTabViewHeader>

      <S.Divider />

      <S.ProductDetailTabContent>
        <S.SubtitleItem>상품 설명</S.SubtitleItem>
        <S.SizedBox height="18.13px" />
        <ProductDescriptionBox />
        <S.SizedBox height="25px" />
        <S.SubtitleItem>상품 옵션</S.SubtitleItem>
        <S.SizedBox height="18.13px" />
        <OptionsListBox />
        <S.SizedBox height="25px" />
        <S.Divider />
        <S.SizedBox height="25px" />
        <PickupDetailBox />
        <DeliveryDetailBox />
      </S.ProductDetailTabContent>
    </section>
  );
};

const PickupDetailBox = () => {
  const { isLoading, data: product } = useProductDetails();

  if (isLoading || !product || !product.pickupInfo) return <></>;

  return (
    <>
      <S.SubtitleItem>픽업 일시</S.SubtitleItem>
      <S.SizedBox height="18.13px" />
      <S.Text4>{product.pickupInfo.dateTimeDuration}</S.Text4>
      <S.SizedBox height="25px" />
      <S.SubtitleItem>픽업 장소</S.SubtitleItem>
      <S.SizedBox height="18.13px" />
      <S.Text4>{product.pickupInfo.locations}</S.Text4>
      <S.SizedBox height="25px" />
      <S.SubtitleItem>픽업 정보</S.SubtitleItem>
      <S.SizedBox height="18.13px" />
      <S.Text4>{product.pickupInfo.note}</S.Text4>
    </>
  );
};

const DeliveryDetailBox = () => {
  const { isLoading, data: product } = useProductDetails();

  if (isLoading || !product || !product.deliveryInfo) return <></>;

  const { deliveryInfo } = product;

  return (
    <>
      <S.SubtitleItem>배송 예정일</S.SubtitleItem>
      <S.SizedBox height="18.13px" />
      <S.Text4>{deliveryInfo.dateDuration}</S.Text4>
      <S.SizedBox height="25px" />
      <S.SubtitleItem>배송 정보</S.SubtitleItem>
      <S.SizedBox height="18.13px" />
      <S.Text4>{deliveryInfo.note}</S.Text4>
    </>
  );
};

const OptionsListBox = () => {
  const { isLoading, data: product } = useProductDetails();

  if (isLoading || !product) return <></>;

  return (
    <>
      {product.optionsText.map((option, idx) => (
        <div key={idx}>
          <S.Text4>{option}</S.Text4>
        </div>
      ))}
    </>
  );
};

import styled from "styled-components";
import { Text4, Title1, Title3 } from "../WTexts";

export const ProductSummarySection = styled.section`
  padding: 10px 20px;
`;

export const BadgeAndPurchaseCountGroupBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
`;

export const DirectShopPickupProductBadgeImage = styled.img`
  width: 96px;
  height: 26.69px;
`;

export const PurchaseCountText = styled.span`
  font-style: normal;
  font-weight: 700;
  font-size: 12px;
  line-height: 18px;

  color: #36b190;
`;

export const ProductTitleText = styled(Title1)`
  color: #2c0e02;
`;

export const PriceText = styled(Title3)`
  display: flex;
  justify-content: flex-end;
  color: #2c0e02;
`;

export const PickupInfoPanelBox = styled.div`
  width: 100%;

  background: #ffffff;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
  border-radius: 15px;

  padding: 6px 10px;
`;

export const ReadCountText = styled(Text4)`
  color: rgba(44, 14, 2, 0.7);
`;

export * from "../WTexts";
export * from "../SizedBox";

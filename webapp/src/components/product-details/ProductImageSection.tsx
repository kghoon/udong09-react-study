import { useProductDetails } from "src/hooks/useProductDetails";
import { Styles } from "src/types/Styles";
import styled from "styled-components";

const S: Styles = {};

S.ImageSection = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  max-height: 640px;
  max-width: 640px;
  width: 100%;
`;

S.ImageBox = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

S.SkeletonBox = styled.div`
  display: flex;
  max-height: 540px;
  max-width: 640px;
  width: 100%;
  height: 100%;
  background-color: gray;
`;

export const ProductImageSection = () => {
  const { isLoading, data: product } = useProductDetails();
  if (isLoading || !product) return <S.SkeletonBox />;

  return (
    <S.ImageSection>
      <S.ImageBox src={product!.photos[0]} alt="" />
    </S.ImageSection>
  );
};

import styled from "styled-components";
import { Styles } from "src/types/Styles";
import { Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box } from "@chakra-ui/react";
import { PropsWithChildren } from "react";
import { useProductDetails } from "src/hooks/useProductDetails";

const S: Styles = {
  ...require("../WTexts"),
  ...require("../Divider"),
};

S.ExpansionPanelListSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

S.ExpansionPanelItemBox = styled.div`
  display: flex;
  flex: 2;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 16px 20px;
`;

export const ExpansionPanelListSection = () => {
  return (
    <S.ExpansionPanelListSection>
      <Accordion allowMultiple>
        <ExpansionItem title="제품 정보">
          <ProductLabelBox />
        </ExpansionItem>
        <ExpansionItem title="픽업 / 배송">
          <PickupInfoBox />
          <DeliveryInfoBox />
        </ExpansionItem>
      </Accordion>
    </S.ExpansionPanelListSection>
  );
};

interface ExpansionItemProps {
  title: string;
}

const ExpansionItem = ({ title, children }: PropsWithChildren<ExpansionItemProps>) => {
  return (
    <AccordionItem>
      <h2>
        <AccordionButton>
          <Box flex="1" textAlign="left" padding="8px 5px">
            <S.Title3>{title}</S.Title3>
          </Box>
          <AccordionIcon fontSize="34px" />
        </AccordionButton>
      </h2>
      <AccordionPanel pb={4}>{children}</AccordionPanel>
    </AccordionItem>
  );
};

S.ExpansionContentListBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  padding: 0px 5px;
`;

S.ExpansionContentItemBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  span {
    padding: 4px 0px;
  }
  span:first-child {
    min-width: 120px;
  }
`;

const ProductLabelBox = () => {
  const { isLoading, data: product } = useProductDetails();
  if (isLoading || !product) return <></>;

  return (
    <S.ExpansionContentListBox>
      {product.labelling.map(([k, v], i) => (
        <S.ExpansionContentItemBox key={i}>
          <S.Title5>{k}</S.Title5>
          <S.Text4>{v}</S.Text4>
        </S.ExpansionContentItemBox>
      ))}
    </S.ExpansionContentListBox>
  );
};

const PickupInfoBox = () => {
  const { isLoading, data: product } = useProductDetails();
  if (isLoading || !product) return <></>;

  if (!product.pickupInfo) {
    return <></>;
  }

  return (
    <S.ExpansionContentListBox>
      <S.ExpansionContentItemBox>
        <S.Title5>픽업 일시</S.Title5>
        <S.Text4>{product.pickupInfo.shortDateTimeDuration}</S.Text4>
      </S.ExpansionContentItemBox>
      <S.ExpansionContentItemBox>
        <S.Title5>픽업 장소</S.Title5>
        <S.Text4>{product.pickupInfo.locations}</S.Text4>
      </S.ExpansionContentItemBox>
      <S.ExpansionContentItemBox>
        <S.Title5>픽업 정보</S.Title5>
        <S.Text4>{product.pickupInfo.note}</S.Text4>
      </S.ExpansionContentItemBox>
    </S.ExpansionContentListBox>
  );
};

const DeliveryInfoBox = () => {
  const { isLoading, data: product } = useProductDetails();
  if (isLoading || !product) return <></>;

  const { deliveryInfo } = product;

  if (!deliveryInfo) {
    return <></>;
  }

  return (
    <S.ExpansionContentListBox>
      <S.ExpansionContentItemBox>
        <S.Title5>배송 일시</S.Title5>
        <S.Text4>{deliveryInfo.dateDuration}</S.Text4>
      </S.ExpansionContentItemBox>
      <S.ExpansionContentItemBox>
        <S.Title5>배송 장소</S.Title5>
        <S.Text4>{deliveryInfo.region}</S.Text4>
      </S.ExpansionContentItemBox>
      <S.ExpansionContentItemBox>
        <S.Title5>배송 정보</S.Title5>
        <S.Text4>{deliveryInfo.note}</S.Text4>
      </S.ExpansionContentItemBox>
    </S.ExpansionContentListBox>
  );
};

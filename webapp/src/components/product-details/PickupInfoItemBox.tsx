import useScreenInfo from "src/hooks/useScreenInfo";
import { Styles } from "src/types/Styles";
import styled from "styled-components";

const S: Styles = {
  ...require("../WTexts"),
  ...require("../SizedBox"),
};

S.PickupInfoItemGroupBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

S.PickupInfoItemTitleText = styled(S.Title5)`
  color: #2c0e02;
  width: 70px;
  min-width: 70px;
`;

S.PickupInfoValueText = styled(S.Text4)`
  color: rgba(44, 14, 2, 0.7);
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  max-width: ${(props) => props.maxWidth ?? 0}px;
`;

export type PickupInfoItemBoxProps = {
  title: string;
  value: string;
};

export const PickupInfoItemBox = ({ title, value }: PickupInfoItemBoxProps) => {
  const { appViewWidth } = useScreenInfo();

  return (
    <S.PickupInfoItemGroupBox>
      <S.PickupInfoItemTitleText>{title}</S.PickupInfoItemTitleText>
      <S.SizedBox width="20px"></S.SizedBox>
      <S.PickupInfoValueText maxWidth={appViewWidth - 150}>{value}</S.PickupInfoValueText>
    </S.PickupInfoItemGroupBox>
  );
};

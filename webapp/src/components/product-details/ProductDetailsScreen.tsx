import React from "react";
import styled from "styled-components";

import { ExpansionPanelListSection } from "./ExpansionPanelListSection";
import { InstallAppButton } from "./InstallAppButton";
import { ProductDetailContentTabViewSection } from "./ProductDetailContentTabViewSection";
import { ProductSummarySection } from "./ProductSummarySection";
import { SellerInfoSection } from "./SellerInfoSection";
import { useProductDetails } from "src/hooks/useProductDetails";
import { Styles } from "src/types/Styles";
import { ProductImageSection } from "./ProductImageSection";

const S: Styles = {
  ...require("../Divider"),
  ...require("../SizedBox"),
};

S.ProductDetailsLayout = styled.div`
  width: 100%;
  background-color: white;
`;

S.BackgroundGap = styled.div<{ height?: string }>`
  background-color: #f5f5f5;
  width: 100%;
  height: ${(props) => props.height || "0px"};
`;

const ProductDetailsScreen = () => {
  const { isLoading } = useProductDetails();

  if (isLoading) {
    return <></>;
  }

  return (
    <S.ProductDetailsLayout>
      <ProductImageSection />
      <SellerInfoSection />
      <S.Divider />
      <ProductSummarySection />
      <S.SizedBox height="20px" />

      <ProductDetailContentTabViewSection />
      <S.BackgroundGap height="10px" />
      <ExpansionPanelListSection />

      <InstallAppButton />
    </S.ProductDetailsLayout>
  );
};

export { ProductDetailsScreen };

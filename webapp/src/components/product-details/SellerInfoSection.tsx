import styled from "styled-components";
import * as WImages from "src/common/WImages";
import { useProductDetails } from "src/hooks/useProductDetails";
import { Styles } from "src/types/Styles";

const S: Styles = {
  ...require("../WTexts"),
  ...require("../SizedBox"),
  ...require("../Avatar"),
};

S.SellerInfoSection = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 9px 19.93px;
`;

S.SellerProfileGroupBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

S.SellerInfoTextGroupBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

S.SellerSectionButtonGroupBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

S.SellerPanelButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;

  box-sizing: border-box;
  width: 120px;
  height: 31px;
  border: 0.2px solid #ffffff;
  border-radius: 5px;

  font-style: normal;
  font-weight: 700;
  font-size: 12px;
  line-height: 18px;
`;

S.MembershipSubscribeButton = styled(S.SellerPanelButton)`
  color: white;
  border: 0.2px solid #ffffff;
  background-color: #2c0e02;
`;

S.AlarmButton = styled.img`
  width: 9.98px;
  height: 8.64px;
`;

S.InquiryButton = styled(S.SellerPanelButton)`
  color: #2c0e02;
  border: 0.2px solid #2c0e02;
  background-color: white;
`;

export const SellerInfoSection = () => {
  const { isLoading, data: product } = useProductDetails();

  if (isLoading || !product) return <></>;

  return (
    <S.SellerInfoSection>
      <S.SellerProfileGroupBox>
        <S.Avatar src={product.seller.image.thumbnail} size="70px" borderThickness="2px" />
        <S.SizedBox width="8px" />

        <S.SellerInfoTextGroupBox>
          <S.Title4>{product.seller.nickname}</S.Title4>
          <S.Text4>{product.deliverySummary}</S.Text4>
        </S.SellerInfoTextGroupBox>
      </S.SellerProfileGroupBox>

      <S.SellerSectionButtonGroupBox>
        <S.MembershipSubscribeButton>
          단골등록
          <S.SizedBox width="6px" />
          <S.AlarmButton src={WImages.iconAlarm} alt="alarm" />
        </S.MembershipSubscribeButton>

        <S.InquiryButton>메시지</S.InquiryButton>
      </S.SellerSectionButtonGroupBox>
    </S.SellerInfoSection>
  );
};

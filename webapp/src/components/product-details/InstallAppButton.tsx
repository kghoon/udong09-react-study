import { Styles } from "src/types/Styles";
import styled from "styled-components";

const S: Styles = {
  ...require("../WTexts"),
};

S.InstallAppButton = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 57px;
  background: #36b190;
  cursor: pointer;
`;

export const InstallAppButton = () => {
  return (
    <S.InstallAppButton>
      <S.Title2 color="white">앱 설치 후 구매하기</S.Title2>
    </S.InstallAppButton>
  );
};

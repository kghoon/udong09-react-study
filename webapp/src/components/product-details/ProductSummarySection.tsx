import * as WImages from "src/common/WImages";
import { PickupInfoItemBox } from "./PickupInfoItemBox";
import { useProductDetails } from "src/hooks/useProductDetails";
import * as S from "./ProductSummarySectionStyles";
import { ProductDetails } from "src/types/ProductDetails";

export const ProductSummarySection = () => {
  const { isLoading, data: product } = useProductDetails();

  if (isLoading || !product) return <></>;

  return (
    <S.ProductSummarySection>
      <S.BadgeAndPurchaseCountGroupBox>
        <S.DirectShopPickupProductBadgeImage src={WImages.directShopBadge} />
        <S.SizedBox width="12px" />
        <S.PurchaseCountText>{product.purchaseCount}건 구매되었어요!</S.PurchaseCountText>
      </S.BadgeAndPurchaseCountGroupBox>

      <S.SizedBox height="5px" />
      <S.ProductTitleText>{product.title}</S.ProductTitleText>
      <S.PriceText>{product.priceText}</S.PriceText>
      <S.SizedBox height="15px" />

      <S.PickupInfoPanelBox>
        <PickupInfoBox product={product} />
        <DeliveryInfoBox product={product} />
      </S.PickupInfoPanelBox>

      <S.SizedBox height="5px" />
      <S.ReadCountText>조회 {product.viewCount}</S.ReadCountText>
    </S.ProductSummarySection>
  );
};

interface PickupInfoBoxProps {
  product: ProductDetails;
}

const PickupInfoBox = ({ product }: PickupInfoBoxProps) => {
  if (!product.pickupInfo) return <></>;

  return (
    <>
      <PickupInfoItemBox title="픽업 일시" value={product.pickupInfo.date} />
      <S.SizedBox height="8px" />
      <PickupInfoItemBox title="픽업 장소" value={product.pickupInfo.locations} />
      <S.SizedBox height="8px" />
      <PickupInfoItemBox title="픽업 정보" value={product.pickupInfo.note} />
    </>
  );
};

interface DeliveryInfoBoxProps {
  product: ProductDetails;
}

const DeliveryInfoBox = ({ product }: DeliveryInfoBoxProps) => {
  const { deliveryInfo } = product;

  if (!deliveryInfo) return <></>;

  return (
    <>
      <PickupInfoItemBox title="배송 예정일" value={deliveryInfo.dateDuration} />
      <S.SizedBox height="8px" />
      <PickupInfoItemBox title="배송 지역" value={deliveryInfo.region} />
      <S.SizedBox height="8px" />
      <PickupInfoItemBox title="픽업 정보" value={deliveryInfo.note} />
    </>
  );
};

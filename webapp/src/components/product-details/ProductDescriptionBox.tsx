import { useEffect, useRef } from "react";
import { useProductDetails } from "src/hooks/useProductDetails";
import useScreenInfo from "src/hooks/useScreenInfo";
import { Styles } from "src/types/Styles";
import styled from "styled-components";

const S: Styles = {};

S.ProductDescriptionBox = styled.div<{ width: string }>`
  max-width: ${(props) => props.width}px;
  width: ${(props) => props.width}px;
  overflow: hidden;

  img {
    max-width: ${(props) => props.width}px;
  }
`;

export const ProductDescriptionBox = () => {
  const { isLoading, data: product } = useProductDetails();
  const containerRef = useRef<HTMLDivElement | null>(null);
  const { appViewWidth } = useScreenInfo();

  const outerHorizontalPadding = 20;
  const maxWidth = appViewWidth - outerHorizontalPadding * 2;

  useEffect(() => {
    if (containerRef.current && product) {
      containerRef.current.innerHTML = product.htmlDescription;
    }
  }, [containerRef, product]);

  if (isLoading || !product) return <></>;
  return <S.ProductDescriptionBox ref={containerRef} width={maxWidth} />;
};

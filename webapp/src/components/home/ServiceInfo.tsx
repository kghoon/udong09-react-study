import styled from "styled-components";
import * as WImages from "src/common/WImages";
import { Styles } from "src/types/Styles";

const S: Styles = {
  ...require("../WTexts"),
  ...require("../SizedBox"),
};

S.ServiceInfoSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  background-color: #fdf5f3;
  width: 450px;
`;

export const ServiceInfo = () => {
  return (
    <S.ServiceInfoSection>
      <S.Title1>우리동네 공동구매</S.Title1>
      <S.SizedBox height="10px" />
      <img src={WImages.logo} width="300px" alt="logo" />
      <S.SizedBox height="50px" />
      <S.Text2>
        줄서서 먹는 전국 맛집, 제철과일과 수산물,
        <br />
        육아용품까지 멀리가서 힘들게 사오지 마세요.
        <br />
        이제 집앞에서 편리하게 픽업하세요.
      </S.Text2>
    </S.ServiceInfoSection>
  );
};

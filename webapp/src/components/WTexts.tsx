import styled from "styled-components";

const BaseTitle = styled.span<{ color?: string }>`
  font-style: normal;
  font-weight: 700;
  color: ${(props) => props.color || "#2C0E02"};
`;

export const Title1 = styled(BaseTitle)`
  font-size: 24px;
  line-height: 28px;
`;

export const Title2 = styled(BaseTitle)`
  font-size: 20px;
  line-height: 30px;
`;

export const Title3 = styled(BaseTitle)`
  font-size: 18px;
  line-height: 27px;
`;

export const Title4 = styled(BaseTitle)`
  font-size: 16px;
  line-height: 20px;
`;

export const Title5 = styled(BaseTitle)`
  font-size: 14px;
  line-height: 20px;
`;

const BaseText = styled.span<{ color?: string }>`
  font-style: normal;
  font-weight: 400;
  color: ${(props) => props.color || "#2C0E02"};
`;

export const Text1 = styled(BaseText)`
  font-size: 20px;
  line-height: 28px;
`;

export const Text2 = styled(BaseText)`
  font-size: 18px;
  line-height: 26px;
`;

export const Text3 = styled(BaseText)`
  font-size: 16px;
  line-height: 24px;
`;

export const Text4 = styled(BaseText)`
  font-size: 14px;
  line-height: 22px;
`;

export const Text5 = styled(BaseText)`
  font-size: 12px;
  line-height: 22px;
`;

import styled from "styled-components";

export interface AvatarProps {
  src: string;
  size: string;
  borderColor?: string;
  borderThickness?: number;
}

export const Avatar = styled.div<AvatarProps>`
  width: ${(props) => props.size ?? "70px"};
  height: ${(props) => props.size ?? "70px"};
  background-image: url(${(props) => props.src});
  background-position: center;
  background-size: cover;
  border-radius: 50%;
  border-style: solid;
  border-color: ${(props) => props.borderColor ?? "#36B190"};
  border-width: ${(props) => props.borderThickness ?? "0px"};
`;

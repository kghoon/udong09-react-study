import styled from "styled-components";
import * as WImages from "./WImages";

const StyledBaseTitle = styled.span`
  font-style: normal;
  font-weight: 700;
  color: ${(props) => props.color || "#2C0E02"};
`;

export const StyledTitle1 = styled(StyledBaseTitle)`
  font-size: 24px;
  line-height: 28px;
`;

export const StyledTitle2 = styled(StyledBaseTitle)`
  font-size: 20px;
  line-height: 30px;
`;

export const StyledTitle3 = styled(StyledBaseTitle)`
  font-size: 18px;
  line-height: 27px;
`;

export const StyledTitle4 = styled(StyledBaseTitle)`
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 20px;
`;

export const StyledTitle5 = styled(StyledBaseTitle)`
  font-style: normal;
  font-weight: 700;
  font-size: 12px;
  line-height: 20px;
`;

const StyledBaseText = styled.span`
  font-style: normal;
  font-weight: 400;
  color: ${(props) => props.color || "#2C0E02"};
`;

export const StyledText3 = styled(StyledBaseText)`
  font-size: 14px;
  line-height: 18px;
`;

export const StyledText4 = styled(StyledBaseText)`
  font-size: 12px;
  line-height: 18px;
`;

export const SizedBox = styled.div`
  width: ${(props) => props.width || "0px"};
  height: ${(props) => props.height || "0px"};
`;

export const Divider = styled.div`
  height: 0px;
  width: 100%;
  border-top: 0.1px solid rgba(0, 0, 0, 0.1);
`;

export { WImages };

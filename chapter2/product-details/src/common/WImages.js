export const avatar = "./images/avatar@1.5x.jpg";
export const contentProductImage = "./images/content_product_image@1.5x.jpg";
export const directShopBadge = "./images/direct_shop_badge@1.5.png";
export const iconAlarm = "./images/icon_alarm@1.5x.png";
export const iconArrow = "./images/icon_arrow@1.5x.png";
export const productImage = "./images/product_image@1.5x.jpg";

import "./App.css";

import { ProductDetailsScreen } from "./ProductDetailsScreen";

function App() {
  return <ProductDetailsScreen />;
}

export default App;

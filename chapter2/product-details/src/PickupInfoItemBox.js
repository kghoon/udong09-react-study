import styled from "styled-components";
import { SizedBox, StyledText4, StyledTitle5 } from "./common";

const StyledPickupInfoItemGroupBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

const StyledPickupInfoItemTitleText = styled(StyledTitle5)`
  color: #2c0e02;
`;

const StyledPickupInfoValueText = styled(StyledText4)`
  color: rgba(44, 14, 2, 0.7);
`;

export const PickupInfoItemBox = ({ title, value }) => {
  return (
    <StyledPickupInfoItemGroupBox>
      <StyledPickupInfoItemTitleText>{title}</StyledPickupInfoItemTitleText>
      <SizedBox width="20px"></SizedBox>
      <StyledPickupInfoValueText>{value}</StyledPickupInfoValueText>
    </StyledPickupInfoItemGroupBox>
  );
};

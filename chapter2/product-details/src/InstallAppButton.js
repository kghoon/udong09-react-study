import styled from "styled-components";
import { StyledTitle2 } from "./common";

const StyledInstallAppButton = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 57px;
  background: #36b190;
  cursor: pointer;
`;

export const InstallAppButton = () => {
  return (
    <StyledInstallAppButton>
      <StyledTitle2 color="white">설치하기</StyledTitle2>
    </StyledInstallAppButton>
  );
};

import styled from "styled-components";
import * as WImages from "./common/WImages";
import { SizedBox, StyledText4, StyledTitle4 } from "./common";

const StyledSellerInfoSection = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 9px 19.93px;
`;

const StyledSellerProfileGroupBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

const StyledSellerInfoTextGroupBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const StyledSellerSectionButtonGroupBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const StyledSellerPanelButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;

  box-sizing: border-box;
  width: 120px;
  height: 31px;
  border: 0.2px solid #ffffff;
  border-radius: 5px;

  font-style: normal;
  font-weight: 700;
  font-size: 12px;
  line-height: 18px;
`;

const StyledMembershipSubscribeButton = styled(StyledSellerPanelButton)`
  color: white;
  border: 0.2px solid #ffffff;
  background-color: #2c0e02;
`;

const StyledAlarmButton = styled.img`
  width: 9.98px;
  height: 8.64px;
`;

const StyledInquiryButton = styled(StyledSellerPanelButton)`
  color: #2c0e02;
  border: 0.2px solid #2c0e02;
  background-color: white;
`;

export const SellerInfoSection = () => {
  return (
    <StyledSellerInfoSection>
      <StyledSellerProfileGroupBox>
        <img src={WImages.avatar} alt="seller avatar" width="70px" height="70px" />
        <SizedBox width="8px" />

        <StyledSellerInfoTextGroupBox>
          <StyledTitle4>해피해피</StyledTitle4>
          <StyledText4>망원동</StyledText4>
        </StyledSellerInfoTextGroupBox>
      </StyledSellerProfileGroupBox>

      <StyledSellerSectionButtonGroupBox>
        <StyledMembershipSubscribeButton>
          단골등록
          <SizedBox width="6px" />
          <StyledAlarmButton src={WImages.iconAlarm} alt="alarm" />
        </StyledMembershipSubscribeButton>

        <StyledInquiryButton>메시지</StyledInquiryButton>
      </StyledSellerSectionButtonGroupBox>
    </StyledSellerInfoSection>
  );
};

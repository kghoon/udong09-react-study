import styled from "styled-components";
import * as WImages from "./common/WImages";
import { SizedBox, StyledText4, StyledTitle1, StyledTitle3 } from "./common";
import { PickupInfoItemBox } from "./PickupInfoItemBox";

const StyledProductSummarySection = styled.section`
  padding: 10px 20px;
`;

const StyledBadgeAndPurchaseCountGroupBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
`;

const StyledDirectShopPickupProductBadgeImage = styled.img`
  width: 96px;
  height: 26.69px;
`;

const StyledPurchaseCountText = styled.span`
  font-style: normal;
  font-weight: 700;
  font-size: 12px;
  line-height: 18px;

  color: #36b190;
`;

const StyledProductTitleText = styled(StyledTitle1)`
  color: #2c0e02;
`;

const StyledPriceText = styled(StyledTitle3)`
  display: flex;
  justify-content: flex-end;
  color: #2c0e02;
`;

const StyledPickupInfoPanelBox = styled.div`
  width: 335px;

  background: #ffffff;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
  border-radius: 15px;

  padding: 6px 10px;
`;

const StyledReadCountText = styled(StyledText4)`
  color: rgba(44, 14, 2, 0.7);
`;

export const ProductSummarySection = () => {
  return (
    <StyledProductSummarySection>
      <StyledBadgeAndPurchaseCountGroupBox>
        <StyledDirectShopPickupProductBadgeImage src={WImages.directShopBadge} />
        <SizedBox width="12px" />
        <StyledPurchaseCountText>1,460건 구매되었어요!</StyledPurchaseCountText>
      </StyledBadgeAndPurchaseCountGroupBox>

      <SizedBox height="5px" />
      <StyledProductTitleText>이이이이 무슨 일이야 신토불이 떡볶이 너무 맛</StyledProductTitleText>
      <StyledPriceText>11,000원~</StyledPriceText>
      <SizedBox height="15px" />

      <StyledPickupInfoPanelBox>
        <PickupInfoItemBox title="픽업 일시" value="2022.03.15 (화)" />
        <SizedBox height="8px" />
        <PickupInfoItemBox title="픽업 장소" value="지역별 상이" />
        <SizedBox height="8px" />
        <PickupInfoItemBox title="픽업 정보" value="당일 픽업 (다음날까지 보관)" />
      </StyledPickupInfoPanelBox>

      <SizedBox height="5px" />
      <StyledReadCountText>조회 150</StyledReadCountText>
    </StyledProductSummarySection>
  );
};

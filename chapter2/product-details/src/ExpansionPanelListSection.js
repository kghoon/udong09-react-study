import styled from "styled-components";
import { Divider, StyledTitle3, WImages } from "./common";

const StyledExpansionPanelListSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const StyledExpansionPanelItemBox = styled.div`
  display: flex;
  flex: 2;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 16px 20px;
`;

export const ExpansionPanelListSection = () => {
  return (
    <StyledExpansionPanelListSection>
      <StyledExpansionPanelItemBox>
        <StyledTitle3>제품 정보</StyledTitle3>
        <ExpansionPanelDirection />
      </StyledExpansionPanelItemBox>

      <Divider />

      <StyledExpansionPanelItemBox>
        <StyledTitle3>픽업 / 배송</StyledTitle3>
        <ExpansionPanelDirection />
      </StyledExpansionPanelItemBox>
    </StyledExpansionPanelListSection>
  );
};

const Rotate = styled.span`
  transform: ${(props) => "rotate(" + props.value + ")" ?? "rotate(0deg)"};
`;

const ExpansionPanelDirection = ({ opened }) => {
  return (
    <Rotate value={opened ? "180deg" : "0deg"}>
      <img src={WImages.iconArrow} alt="arrow" height="11px" width="17.06px" />
    </Rotate>
  );
};

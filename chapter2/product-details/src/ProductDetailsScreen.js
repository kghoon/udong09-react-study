import React from "react";
import styled from "styled-components";

import { SizedBox, Divider, WImages } from "./common";
import { ExpansionPanelListSection } from "./ExpansionPanelListSection";
import { InstallAppButton } from "./InstallAppButton";
import { ProductDetailContentTabViewSection } from "./ProductDetailContentTabViewSection";
import { ProductSummarySection } from "./ProductSummarySection";
import { SellerInfoSection } from "./SellerInfoSection";

const StyledProductDetailsLayout = styled.div`
  max-width: 375px;
  min-width: 375px;
  background-color: white;
`;

const StyledBackgroundGap = styled.div`
  background-color: #f5f5f5;
  width: 100%;
  height: ${(props) => props.height || "0px"};
`;

const ProductDetailsScreen = () => {
  return (
    <StyledProductDetailsLayout>
      <img src={WImages.productImage} alt="" width="375px" height="375px" />
      <SellerInfoSection />
      <Divider />
      <ProductSummarySection />
      <SizedBox height="20px" />

      <ProductDetailContentTabViewSection />
      <StyledBackgroundGap height="10px" />
      <ExpansionPanelListSection />

      <InstallAppButton />
    </StyledProductDetailsLayout>
  );
};

export { ProductDetailsScreen };

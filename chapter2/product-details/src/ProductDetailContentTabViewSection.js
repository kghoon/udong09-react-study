import styled from "styled-components";
import { SizedBox, Divider, StyledText3, WImages } from "./common";

const StyledProductDetailTabViewHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  padding: 0px 20px;
`;

const StyledProductDetailTabViewItem = styled.div`
  height: 27px;

  /* Title 03 */
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 27px;
  /* identical to box height */

  text-align: center;
  border-bottom: 3px solid #36b190;
  border-bottom-width: ${(props) => (props.active ? "3px" : "0px")};
`;

const StyledProductDetailTabContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;

  padding: 39px 20px;
  box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
`;

const StyledSubtitleItem = styled.div`
  display: flex;

  height: 27px;

  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 27px;

  text-align: center;
  border-bottom: 3px solid #2c0e02;
`;

export const ProductDetailContentTabViewSection = () => {
  return (
    <section>
      <StyledProductDetailTabViewHeader>
        <StyledProductDetailTabViewItem active>상세정보</StyledProductDetailTabViewItem>
        <SizedBox width="36px" />
        <StyledProductDetailTabViewItem>댓글 0</StyledProductDetailTabViewItem>
      </StyledProductDetailTabViewHeader>

      <Divider />

      <StyledProductDetailTabContent>
        <StyledSubtitleItem>상품 설명</StyledSubtitleItem>
        <SizedBox height="29.31px" />

        <StyledText3>
          한지민 떡볶이로 유명해진 신토불이 떡볶이 😍❤️ <br />
          밀떡으로 쫄깃하고 맵고 칼칼한 맛이에요!
          <br />
          핫도그, 만두와 같이 먹으면 꿀맛이라 1인세트로 주문하시면
          <br />
          한 번에 모두 맛 볼 수 있다 이 말입니다.
          <br />
          순식간에 매진되어 버리니까
          <br />
          고민하지말고 바아로 주문하세요.
        </StyledText3>

        <SizedBox height="40px" />
        <img src={WImages.contentProductImage} alt="product content" width="335px" height="335px" />
        <SizedBox height="25px" />
        <StyledSubtitleItem>상품 옵션</StyledSubtitleItem>
        <SizedBox height="18.13px" />
        <StyledText3>신토불이 떡볶이 세트 11,000원</StyledText3>
        <SizedBox height="25px" />
        <Divider />
        <SizedBox height="25px" />
        <StyledSubtitleItem>픽업 일시</StyledSubtitleItem>
        <SizedBox height="18.13px" />
        <StyledText3>2021년 4월 5일 14 ~ 14시 30분</StyledText3>
        <SizedBox height="25px" />
        <StyledSubtitleItem>픽업 장소</StyledSubtitleItem>
        <SizedBox height="18.13px" />
        <StyledText3>풍덕천점, 정자점, 죽전점, 동탄목동점, 동탄푸른점</StyledText3>
        <SizedBox height="25px" />
        <StyledSubtitleItem>픽업 정보</StyledSubtitleItem>
        <SizedBox height="18.13px" />
        <StyledText3>당일 픽업 (다음날 까지 보관)</StyledText3>
      </StyledProductDetailTabContent>
    </section>
  );
};
